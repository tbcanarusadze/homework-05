package com.example.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.animation.AnimationUtils
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.emailField
import kotlinx.android.synthetic.main.activity_main.passwordField
import kotlinx.android.synthetic.main.activity_main2.*
import java.util.regex.Pattern
import kotlin.math.sign

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        animations()
        init()
    }

    private fun init() {
        logInButton.setOnClickListener {
            emptyTextFields()
        }
        forgotPassword.setOnClickListener {
            Toast.makeText(applicationContext, "Ups! Sorry for U", Toast.LENGTH_SHORT).show()
        }
        signUpTextView.setOnClickListener {
            val intent = Intent(this, Main2Activity::class.java)
            startActivity(intent)
        }
    }

    private fun emptyTextFields() {
        if (emailField.text.isNotEmpty() && passwordField.text.isNotEmpty()) {
            checkEmail()
        } else {
            Toast.makeText(applicationContext, "Please fill all fields!", Toast.LENGTH_SHORT).show()
        }

    }

    private fun checkEmail() {
        Toast.makeText(
            applicationContext,
            if (isEmailValid(emailField.text.toString())) "Log In Success" else "invalid Email",
            Toast.LENGTH_SHORT
        ).show()

    }

    private fun animations() {
        val animation = AnimationUtils.loadAnimation(this, R.anim.zoom_in)
        logInTitle.startAnimation(animation)
        val animation2 = AnimationUtils.loadAnimation(this, R.anim.fade_in)
        emailField.startAnimation(animation2)
        passwordField.startAnimation(animation2)
        forgotPassword.startAnimation(animation2)
        val animation3 = AnimationUtils.loadAnimation(this, R.anim.fade_in2)
        logInButton.startAnimation(animation3)
        accountField.startAnimation(animation3)

    }

    private fun isEmailValid(email: String): Boolean {
        return Pattern.compile(
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9]))|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(email).matches()
    }

}
