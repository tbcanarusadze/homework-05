package com.example.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.animation.AnimationUtils
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.emailField
import kotlinx.android.synthetic.main.activity_main.passwordField
import kotlinx.android.synthetic.main.activity_main2.*
import java.util.regex.Pattern

class Main2Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        animations()
        init()
    }

    private fun init() {
        signUpButton.setOnClickListener {
            emptyTextFields()
        }
        logInTextView.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)

        }
    }

    private fun emptyTextFields() {
        if (emailField.text.isNotEmpty() && passwordField2.text.isNotEmpty() && repeatPasswordField.text.isNotEmpty()) {
            checkEmail()
        } else {
            Toast.makeText(applicationContext, "Please fill all fields!", Toast.LENGTH_SHORT).show()
        }

    }

    private fun checkEmail() {
        if (isEmailValid(emailField.text.toString())) {
            checkPasswords()
        } else {
            Toast.makeText(applicationContext, "invalid Email", Toast.LENGTH_SHORT).show()
        }
    }


    private fun checkPasswords() {

        Toast.makeText(
            applicationContext,
            if (passwordField2.text.toString() != repeatPasswordField.text.toString()) "Passwords don't match" else "Sign Up Success",
            Toast.LENGTH_SHORT
        ).show()

    }

    private fun animations() {
        val animation = AnimationUtils.loadAnimation(this, R.anim.zoom_in)
        signUpTitle.startAnimation(animation)
        val animation2 = AnimationUtils.loadAnimation(this, R.anim.fade_in)
        emailField.startAnimation(animation2)
        passwordField2.startAnimation(animation2)
        repeatPasswordField.startAnimation(animation2)
        val animation3 = AnimationUtils.loadAnimation(this, R.anim.fade_in2)
        signUpButton.startAnimation(animation3)
        accountField2.startAnimation(animation3)

    }

    private fun isEmailValid(email: String): Boolean {
        return Pattern.compile(
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9]))|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(email).matches()
    }


}
